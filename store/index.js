import Vuex from 'vuex'
import cards from '@/data/cards'
import { createClient } from '~/plugins/contentful.js'

const client = createClient()

const createStore = () => {
  return new Vuex.Store({
    state: {
      highlights: [],
      categoriesArray: [],
      categories: {
        eyes: [],
        lips: [],
        base: []
      },
      category: [],
      feed: [],
      productIndex: null,
      feedLength: null,
      menuModalOpen: false,
      productModal: {
        show: false,
        image: ''
      },
      frontPage: {
        highlightText: '',
        week: ''
      },
      aboutPage: null,
      contactPage: null,
      firstProductImage: 0,
      inverseLogo: false
    },
    mutations: {
      setInverseLogo(state, payload) {
        return state.inverseLogo = payload
      },
      setAboutPage(state, payload) {
        return state.aboutPage = payload
      },
      setContactPage(state, payload) {
        return state.contactPage = payload
      },
      setFrontPage(state, payload) {
        return state.frontPage = payload
      },
      setHighlights(state, payload) {
        return state.highlights = payload
      },
      setCategories(state, payload) {
        return state.categories = payload
      },
      setCategoriesArray(state, payload) {
        return state.categoriesArray = payload
      },
      setFeed(state, payload) {
        return state.feed = payload
      },
      setCategory(state, payload) {
        return state.category = payload
      },
      setProductIndex(state, payload) {
        return state.productIndex = payload
      },
      setFirstProductImage(state, payload) {
        return state.firstProductImage = payload
      },
      openModalMenu(state, payload) {
        return state.menuModalOpen = true
      },
      closeModalMenu(state, payload) {
        return state.menuModalOpen = false
      },
      setFeedLength(state, payload) {
        return state.feedLength = payload
      },
      setProductModal(state, payload) {
        return state.productModal = {
          show: payload.show,
          image: payload.image,
        }
      }
    },
    getters: {
      getInverseLogo(state) {
        return state.inverseLogo
      },
      getAboutPage(state,) {
        return state.aboutPage
      },
      getContactPage(state,) {
        return state.contactPage
      },
      getFrontPage(state) {
        return state.frontPage
      },
      getHighlights(state) {
        return state.highlights
      },
      getCategory(state) {
        return state.category
      },
      getCategories(state) {
        return state.categories
      },
      getCategoriesArray(state) {
        return state.categoriesArray
      },
      getFeed(state) {
        return state.feed
      },
      getProductIndex(state) {
        return state.productIndex
      },
      getMenuModalState(state) {
        return state.menuModalOpen
      },
      getFirstProductImage(state) {
        return state.firstProductImage
      },
      getFeedLength(state,) {
        return state.feedLength
      },
      getProductModalState(state) {
        return state.productModal
      }
    },
    actions: {
      fetchFrontPage(ctx) {
        return client.getEntries({
          content_type: 'frontPage',
          order: '-sys.updatedAt',
          include: 2
        })
          .then( response => {

            let responseCards = response.items[0].fields.highlights.fields.highlights
            let positions = response.items[0].fields.highlights.fields.position
            if (responseCards.length > positions.length) {
              const extra = responseCards.length - positions.length
              const arr = new Array(extra).fill(0);
              positions = [ ... positions, ...arr]
            }
            responseCards = responseCards.map( (card, i) => {
              return {
                img: card.fields.image.fields.file.url,
                title: card.fields.title,
                author: card.fields.author,
                link: card.fields.link,
                position: positions[i]
              }
            })

            let arr = []
            responseCards.forEach((responseCard, i) => {
              let obj = {
                ...cards[i],
                ...responseCard
              }
              arr.push(obj)
            })
            ctx.commit('setHighlights', arr)

            ctx.commit('setFrontPage', response.items[0].fields)
          })
      },
      fetchAboutPage(ctx) {
        return client.getEntries({
          content_type: 'aboutPage',
          order: '-sys.updatedAt'
        })
          .then( response => {
            ctx.commit('setAboutPage', response.items[0].fields)
          })
      },
      fetchContactPage(ctx) {
        return client.getEntries({
          content_type: 'contactPage',
          order: '-sys.updatedAt'
        })
          .then( response => {
            ctx.commit('setContactPage', response.items[0].fields)
          })
      },
      fetchCategories(ctx) {
        return client.getEntries({
          content_type: 'productCategory'
        })
        .then( response => {
          let obj = JSON.parse(JSON.stringify(ctx.state.categories, {}))

          for (let key in obj) {
            obj[key] = []
          }
          for (let key in obj) {
            response.items.forEach(item => {
              if (key === item.fields.facePart.toLowerCase()) {
                obj[key].push(item.fields.categoryName)
              }
            })
          }
          ctx.commit('setCategories', obj)
          ctx.commit('setCategoriesArray', response.items)
        })
      },
      categoryTypeFormatter(ctx, {categoryType, direction}) {
        let fromArray,
          toArray
        const slug = ['eyes', 'base', 'lips']
        const english = ['Eyes', 'Base', 'Lips']
        if (direction === 'to english') {
          fromArray = slug
          toArray = english
        } else if (direction === 'to slug') {
          fromArray = english
          toArray = slug
        }
        if (fromArray.some(x => x === categoryType)) {
          const pos = fromArray.indexOf(categoryType)
          return toArray[pos]
        } else {
          return undefined
        }
      },
      async fetchProducts(ctx, input) {
        const obj = {
          category: undefined,
          categoryType: undefined,
          ...input
        }
        const { category, categoryType } = obj
        const name = await ctx.dispatch('categoryTypeFormatter', {
          categoryType,
          direction: 'to english'
        })


        if (!name && category) {
          return false
        }

        return client.getEntries({
          content_type: 'product',
          'fields.productCategory.sys.contentType.sys.id': 'productCategory',
          'fields.productCategory.fields.facePart': name,
          'fields.productCategory.fields.slug': category,
          order: '-sys.createdAt'
        })
          .then( response => {

            let obj = response.items.map( item => {
              return {
                sys: item.sys,
                ...item.fields,
                createdAt: item.sys.createdAt
              }
            })

            ctx.commit('setFeedLength', 3)
            ctx.commit('setCategory', obj)
            return true
          })
          .catch(e => {
            return false
          })
      },
    }
  })
}

export default createStore
