const { getRoutes } = require('./services/getRoutes')
const pkg = require('./package')
const contentfulConfig = require('./.contentful.json')
const nodeExternals = require('webpack-node-externals')
const features = [
  'Set',
  'Array.from',
  'CustomEvent'
].join('%2C')
let mode = process.env.NODE_ENV === 'preview' ? 'spa' : 'universal'

module.exports = {
  mode: mode,
  /*
  ** Headers of the page
  */
  head: {
    title: 'Your Palette',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'pkg.description' }
    ],
    script: [
      { src: `https://polyfill.io/v3/polyfill.min.js?features=${features}`, body: true },
      { src: 'https://unpkg.com/marked@0.3.6', body: true  }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },
/*
  ** Customize the progress-bar color
  */
  loading: { color: '#FFFFFF' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/app.scss',
    '@/assets/css/media-queries.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-scroll.js',
    {src: '~/plugins/youtube-embed', ssr: false},

    // {src: '~plugins/vue-markdown', ssr: true},
    {src: '~plugins/moment', ssr: true},
    {src: '~plugins/social', ssr: true},
    // {src: '~plugins/zoom', ssr: false}
  ],
  /*
  ** Nuxt.js modules
  */

  modules: [
    [
      'nuxt-mq',
      {
        // Default breakpoint for SSR
        defaultBreakpoint: 'default',
        breakpoints: {
          sm: 576,
          md: 1250,
          lg: Infinity
        }
      }
    ],
    // '@nuxtjs/moment'
  ],

  /*
  ** Build configuration
  */
  router: {
    middleware: 'check-modal-open'
  },
  generate: {
    routes() {
      return getRoutes()
    },
    fallback: true
  },
  env: {
    CTF_SPACE_ID: contentfulConfig.CTF_SPACE_ID,
    CTF_CDA_ACCESS_TOKEN: contentfulConfig.CTF_CDA_ACCESS_TOKEN,
    CTF_CPA_ACCESS_TOKEN: contentfulConfig.CTF_CPA_ACCESS_TOKEN,
    PREVIEW: process.env.NODE_ENV === 'preview',
  },
  build: {
    // transpile: ['vue-product-zoomer']
    // vendor:[ 'vue-product-zoomer']
    /*
    ** You can extend webpack config here
    */
    // extend(config, ctx) {
    //   // Run ESLint on save
    //   if (ctx.isDev && ctx.isClient) {
    //     config.module.rules.push({
    //       enforce: 'pre',
    //       test: /\.(js|vue)$/,
    //       loader: 'eslint-loader',
    //       exclude: /(node_modules)/
    //     })
    //   }
    //   if (ctx.isServer) {
    //     config.externals = [
    //       nodeExternals({
    //       })
    //     ]
    //   }
    // }
  }
}
