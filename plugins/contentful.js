const contentful = require('contentful')

const createClient = () => {
  let config

  if (process.env.PREVIEW) {
    config = {
      space: process.env.CTF_SPACE_ID,
      accessToken: process.env.CTF_CPA_ACCESS_TOKEN,
      host: 'preview.contentful.com'
    }
  } else {
    config = {
      space: process.env.CTF_SPACE_ID,
      accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
    }
  }

  return contentful.createClient(config)

}
module.exports = {
  createClient
}
