export default [
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    title: 'Title 1',
    author: 'Sophia Nilsson',
    class: 'rollercoaster__card--1',
    transformImage: 'perspective(100rem) rotateY(-24deg)',
    transformText: 'rotate(3.5deg) translateX(20px) translateY(3px)',
    width: '312px',
    height: '377px'
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--2',
    transformImage: 'skewY(-10deg)',
    transformText: 'rotate(-9deg) translateY(-15px)',
    float: 'right',
    width: '290px',
    height: '290px'
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--3',
    transformImage: 'perspective(100rem) rotateY(0)',
    width: '363px',
    height: '243px',
    position: 20,
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--4',
    crop: 'object-fit: cover',
    width: '288px',
    height: '288px',
    transformImage: 'rotate(90deg)',
    transformText: 'translateY(5px)',
    position: 10,

  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--5',
    transformImage: 'skewY(-8deg)',
    transformText: 'rotate(-7deg) translateY(22px)',
    width: '288px',
    height: '365px',
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--6',
    float: 'right',
    width: '288px',
    height: '365px',
    transformText: 'translateY(5px)',
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--7',
    width: '294px',
    height: '361px',
    transformText: 'translateY(5px)',
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--8',
    float: 'right',
    width: '360px',
    height: '242px',
    transformText: 'translateY(5px)',
    position: 10,
  },
  {
    img: 'https://avatars.mds.yandex.net/get-pdb/224463/28204942-1e07-4cce-bfa3-237d6bbe5014/orig',
    class: 'rollercoaster__card--9',
    width: '288px',
    height: '288px',
    transformText: 'translateY(5px)',
  }

]

