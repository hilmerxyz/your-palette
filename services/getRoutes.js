const contentful = require('contentful')

const config = {
  CTF_SPACE_ID: 'mn5aznkgdg2f',
  CTF_CDA_ACCESS_TOKEN: '1b4630ae166b839d7e0a63171b2cca8b40005c03aa0625e0ee7e9f2038de4471',
}
const client = contentful.createClient({
  space: config.CTF_SPACE_ID,
  accessToken: config.CTF_CDA_ACCESS_TOKEN
})

Object.defineProperty(Array.prototype, 'flat', {
  value: function(depth = 1) {
    return this.reduce(function (flat, toFlatten) {
      return flat.concat((Array.isArray(toFlatten) && (depth>1)) ? toFlatten.flat(depth-1) : toFlatten);
    }, []);
  }
});

const getProduct = (category) => {
  return client.getEntries({
    content_type: 'product',
    'fields.productCategory.sys.contentType.sys.id': 'productCategory',
    'fields.productCategory.fields.slug': category
  })
    .then( res => res.items.map( item => `products/${category}/${item.fields.slug}` ))
}

const getRoutes = async () => {
  let categories = await client.getEntries({
    content_type: 'productCategory'
  })
    .then(res => res.items.map(category =>  category.fields.slug ))

  let articles = await client.getEntries({
    content_type: 'article'
  })
    .then(res => res.items.map(item =>  {
      const splitDate = item.fields.datePublished.split('-')
      return `articles/${splitDate[0]}/${splitDate[1]}/${item.fields.slug}`
    }))

  let products = await Promise.all(categories.map(getProduct))
    .then(response => response.flat())

  return [...products, ...articles]
}


module.exports = {
  getRoutes
}
